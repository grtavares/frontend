'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('autoprefixer');

const bs = require('browser-sync');
const bsReload = bs.reload;

sass.compiler = require('node-sass');

/*-- Path to folders --*/
const sourceFilesPath = './src';
const scssFilesPath = sourceFilesPath + '/scss';
const jsFilesPath = sourceFilesPath + '/js';
const htmlFilesPath = sourceFilesPath + '/*.html';

/*-- SASS --*/
/* Compile scss files, auto-prefix them and generate sourcemap */
function scss() {
    return gulp.src(scssFilesPath + '/**/*.scss')
        .pipe(sass({
            outputStyle: "nested",
            precision: 5,
            sourceComments: false
        }).on('error', sass.logError))
        .pipe(sourcemaps.init())
        .pipe(postcss([
            autoprefixer({
                grid: "autoplace",
                cascade: false
            })
        ]))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./src/css'))
        .pipe(bsReload({stream: true}));
}

/*-- Browser Sync --*/
function browserSync(done) {
    bs.init({
        server: {
            baseDir: './src/'
        },
        ghostMode: {},
        notify: false
    });

    done();
}

/*-- Watch function --*/
function watchTasks() {
    gulp.watch(scssFilesPath + '/**/*.scss', scss);
    gulp.watch(sourceFilesPath + '/*.html').on('change', bsReload);
    gulp.watch(jsFilesPath + '/**/*.js').on('change', bsReload);
}

/*-- Gulp tasks --*/
gulp.task('scss', scss);
gulp.task('browserSync', browserSync);

gulp.task('default', gulp.series(scss, browserSync, watchTasks));